package com.revature.timesheet.Users;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserVaildation extends Exception{ 

	/**
	 * 
	 */
	private static final long serialVersionUID = -7965989915663731258L;

	private String Message;
	public UserVaildation ()
	{}
	public UserVaildation(String Message)
	{
		super(Message);
		this.Message=Message;
		
	}
	 @Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "Exception has Occured: " + Message;
	}

	public void UserVaildationInput(Users users) throws UserVaildation

	{
		/* check for length of both password and user name 
		 * check that users have at least one digit 
		 * check the password to make  that it has at least one special characters */
		int DigtCount =0;
		String specialCharacters = "(?=.*[@#$%])";
		Pattern passwordpattern = Pattern.compile(specialCharacters);
		Matcher passwordmatch = passwordpattern.matcher(users.getPassWord());

		if(users.getUserName().length()<8||users.getPassWord().length()<9)
		{

			throw new UserVaildation("Username must be 8 ore more charachters long and password must be 9 or more characters long");
			//System.out.println("Username must be 8 ore more charachters long and password must be 9 or more characters long");
			//return false;
		}

		/* once  that is false , check the characters */
		for( int i =0; i<users.getUserName().length(); i++)
		{
			if(Character.isDigit(users.getUserName().charAt(i))) 
			{
				DigtCount++;
			}

		}
		/*Username can only have one digit character */
		if(DigtCount >1)
		{
			 throw new UserVaildation("UserName must have 1 digit character");
			//System.out.println("UserName must have 1 digit character");
			//return false;
		}
		else if(DigtCount<=0)
		{
		//	System.out.println("UserName has no numerical character");
			 throw new UserVaildation("UserName has no numerical character");
			//return false;
			
		}


		/* Check for special character that was enter by the user*/
		boolean specialCharacterFind = passwordmatch.find();
		if(!specialCharacterFind)
		{
			
			//System.out.println("Came here?");
			 throw new UserVaildation("Password must contian at least one special character");

			//return true;
		}
		/*else 
		{
			System.out.println("Password must contian at least one special character");
			return false;
		} */
		


	}

}
