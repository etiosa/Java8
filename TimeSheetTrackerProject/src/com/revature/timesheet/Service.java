package com.revature.timesheet;

import com.revature.timesheet.Users.Users;

public interface Service {

	public boolean logUser(String username, String password);
	public void enterTime(Timesheet ts, Users users);
	public Timesheet viewTime();
}
