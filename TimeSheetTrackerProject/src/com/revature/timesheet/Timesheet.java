package com.revature.timesheet;

import java.util.ArrayList;

import com.revature.timesheet.Users.Users;

public class Timesheet {


	private static Timesheet timesheet;
	private static ArrayList<Users>users;
	
	private Timesheet() {
		super();
	}
	
	public Timesheet getInstance()
	{
		//lazy instanitante
		if( timesheet==null)
		{
			timesheet = new Timesheet();
		}
		return timesheet;
		
	}
	
	@Override
	public String toString() {
		//return "Timesheet [empUser=" + empUser + ", hours=" + hours + ", date=" + date + "]";
		return null;
	}
	public static Timesheet getTimesheet() {
		return timesheet;
	}
	public static void setTimesheet(Timesheet timesheet) {
		Timesheet.timesheet = timesheet;
	}
	public static ArrayList<Users> getUsers() {
		return users;
	}
	public static void setUsers(ArrayList<Users> users) {
		Timesheet.users = users;
	}
	
	
	
}
