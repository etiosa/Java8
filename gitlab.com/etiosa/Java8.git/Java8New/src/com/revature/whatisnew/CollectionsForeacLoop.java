package com.revature.whatisnew;

import java.util.ArrayList;
import java.util.List;

public class CollectionsForeacLoop  {
	public static void main(String [] args)
	{

		List<People>pp = new ArrayList<>();

		People p1 = new People("John", "James", 25);
		People p2 = new People("Hiro", "Okada", 34);
		People p3 = new People("BJ", "Cook", 24);
		People p4 = new People("Cup", "Pen",45);
		People p5 = new People("Roy", "Sky", 56);

		pp.add(p1);
		pp.add(p2);
		pp.add(p3);
		pp.add(p4);
		pp.add(p5);



		// For Loop
		System.out.println("FOR LOOP");
		for(int i=0; i<pp.size();i++)
		{

			System.out.println(pp.get(i));

		}


		//for in Loop
		System.out.println();
		System.out.println("FOR in Loop");
		for(People in: pp)
		{
			System.out.println(in);
		}


		//ForEach Loop
		System.out.println();
		System.out.println("FOREACH ");
		pp.forEach(p->System.out.println(p));
		
		System.out.println();
			
		pp.forEach(System.out::println);

		

	}




}
