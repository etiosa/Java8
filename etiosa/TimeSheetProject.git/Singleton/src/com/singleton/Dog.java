package com.singleton;

import java.util.ArrayList;

public class Dog {
	
	private static Dog mydog;
 private static ArrayList<Food>Dogfood;
	
	private Dog()
	{}
	
	public static Dog getInstance()
	{
		if (mydog ==null)
		{
			 mydog= new Dog();
		}
		return mydog;
	}

	public static ArrayList<Food> getDogfood() {
		return Dogfood;
	}

	public static void setDogfood(ArrayList<Food> dogfood) {
		Dogfood = dogfood;
	}
	

	
 
}
