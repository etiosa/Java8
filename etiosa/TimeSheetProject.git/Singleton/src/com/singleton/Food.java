package com.singleton;

public class Food {
	private String fooname;
	private int foodnumber;
	
	
	public Food(String foodname, int foodnumber)
	{
		this.setFoodnumber(foodnumber);
		this.setFooname(foodname);
	}


	public String getFooname() {
		return fooname;
	}


	public void setFooname(String fooname) {
		this.fooname = fooname;
	}


	public int getFoodnumber() {
		return foodnumber;
	}


	public void setFoodnumber(int foodnumber) {
		this.foodnumber = foodnumber;
	}

}
